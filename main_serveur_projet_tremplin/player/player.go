package player

import (
	"fmt"

	"github.com/bvinc/go-sqlite-lite/sqlite3"
)

//Player struct
type Player struct {
	Name       string  `json:"student"`
	Solve      int     `json:"solve"`
	Count      int     `json:"count"`
	Status     int     `json:"status"`
	OrientateR int     `json:"orientateR"`
	OrientateL int     `json:"orientateL"`
	Time       float64 `json:"time"`
}

//MazeStat struct
type MazeStat struct {
	Name  string `json:"student"`
	Maze  int    `json:"maze"`
	Count int    `json:"count"`
}

//InsertPlayerDB insert the player in the database
func InsertPlayerDB(player Player, db *sqlite3.Conn) {
	stmt, err := db.Prepare("INSERT INTO students VALUES(?, ?, ?, ?, ?, ?, ?);")
	if err != nil {
		fmt.Println(err)
		return
	}
	err = stmt.Exec(player.Name, player.Solve, player.Count, player.Status, player.OrientateR, player.OrientateL, player.Time)
	if err != nil {
		fmt.Println(err)
		return
	}
	stmt.Close()
}

//DeletePlayerDB delete the player from the database
func DeletePlayerDB(player Player, db *sqlite3.Conn) {
	err := db.Exec("DELETE FROM students WHERE id='" + player.Name + "';")
	if err != nil {
		fmt.Println(err)
		return
	}
}

//SelectPlayerDB select the player from the database
func SelectPlayerDB(db *sqlite3.Conn) {
	stmt, err := db.Prepare(`SELECT * FROM students`)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer stmt.Close()
	for {
		hasRow, err := stmt.Step()
		if err != nil {
			fmt.Println(err)
			return
		}
		if !hasRow {
			return
		}
		var (
			name       string
			solve      int
			count      int
			status     int
			orientateR int
			orientateL int
			time       float64
		)
		err = stmt.Scan(&name, &solve, &count, &status, &orientateR, &orientateL, &time)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println("name:", name, "solve:", solve, "count:", count, "status:", status, "orientateR:", orientateR, "orientateL:", orientateL, "time:", time)
	}
}

//GetPlayersDB return a players array
func GetPlayersDB(db *sqlite3.Conn) []Player {
	var players []Player
	stmt, err := db.Prepare(`SELECT * FROM students ORDER BY solve`)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer stmt.Close()
	for {
		hasRow, err := stmt.Step()
		if err != nil {
			fmt.Println(err)
			return nil
		}
		if !hasRow {
			return players
		}
		var (
			name       string
			solve      int
			count      int
			status     int
			orientateR int
			orientateL int
			time       float64
		)
		err = stmt.Scan(&name, &solve, &count, &status, &orientateR, &orientateL, &time)
		if err != nil {
			fmt.Println(err)
			return nil
		}
		players = append(players, Player{name, solve, count, status, orientateR, orientateL, time})
	}
}

//GetMazeStatsDB return a maze stats array
func GetMazeStatsDB(db *sqlite3.Conn) []MazeStat {
	var mazeStats []MazeStat
	stmt, err := db.Prepare(`SELECT * FROM studentsmazestats`)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer stmt.Close()
	for {
		hasRow, err := stmt.Step()
		if err != nil {
			fmt.Println(err)
			return nil
		}
		if !hasRow {
			return mazeStats
		}
		var (
			name  string
			maze  int
			count int
		)
		err = stmt.Scan(&name, &maze, &count)
		if err != nil {
			fmt.Println(err)
			return nil
		}
		mazeStats = append(mazeStats, MazeStat{name, maze, count})
	}
}

//GetMazeStatsByStudentDB return a student maze stats array
func GetMazeStatsByStudentDB(db *sqlite3.Conn, student string) []MazeStat {
	var mazeStats []MazeStat
	stmt, err := db.Prepare(`SELECT * FROM studentsmazestats where id='` + student + "' ORDER BY maze")
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer stmt.Close()
	for {
		hasRow, err := stmt.Step()
		if err != nil {
			fmt.Println(err)
			return nil
		}
		if !hasRow {
			return mazeStats
		}
		var (
			name  string
			maze  int
			count int
		)
		err = stmt.Scan(&name, &maze, &count)
		if err != nil {
			fmt.Println(err)
			return nil
		}
		mazeStats = append(mazeStats, MazeStat{name, maze, count})
	}
}

//InsertStatsDB insert the student maze stat player in the database
func InsertStatsDB(mstats MazeStat, db *sqlite3.Conn) {
	stmt, err := db.Prepare("INSERT INTO studentsmazestats VALUES(?, ?, ?);")
	if err != nil {
		fmt.Println(err)
		return
	}
	err = stmt.Exec(mstats.Name, mstats.Maze, mstats.Count)
	if err != nil {
		fmt.Println(err)
		return
	}
	stmt.Close()
}
