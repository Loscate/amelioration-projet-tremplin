package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"main_serveur_projet_tremplin/handler"
	"main_serveur_projet_tremplin/player"
	"net"
	"net/http"
	"os"

	"github.com/bvinc/go-sqlite-lite/sqlite3"
)

func main() {
	DB, err := sqlite3.Open("database")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	//Création des tables
	err = DB.Exec("CREATE TABLE students(id VARCHAR(100) PRIMARY KEY NOT NULL, solve INT, count INT, status INT, orientateR INT, orientateL INT, time FLOAT);")
	err = DB.Exec("CREATE TABLE studentsmazestats(id VARCHAR(100), maze INT, count INT);")
	listener, err := net.Listen("tcp", ":6666")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("Databse ready")
	go catchConnexion(listener, DB)
	dbForHandler := handler.DBExtends{DB: DB}
	//Définition des routes du serveur HTTP.
	http.HandleFunc("/", handler.MainHandler)
	http.HandleFunc("/api", dbForHandler.APIHandler)
	http.HandleFunc("/api/mazestats", dbForHandler.APIMazeStatsHandler)
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./web_tremplin/assets/"))))
	//Lancement du serveur HTTP.
	log.Fatal(http.ListenAndServe(":8080", nil))

}

func catchConnexion(listener net.Listener, db *sqlite3.Conn) {

	for {
		connexion, err := listener.Accept()
		fmt.Println(connexion.RemoteAddr(), "is connected.")
		if err != nil {
			fmt.Println(err)
		}
		go handleConnexion(connexion, db)
	}
}

func handleConnexion(connexion net.Conn, DB *sqlite3.Conn) {
	defer connexion.Close()
	reader := bufio.NewReader(connexion)
	message, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println(err)
		return
	}
	message2, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Print(message)
	var parray []player.Player
	var marray []player.MazeStat
	err = json.Unmarshal([]byte(message), &parray)
	err = json.Unmarshal([]byte(message2), &marray)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, p := range parray {
		player.DeletePlayerDB(p, DB)
		player.InsertPlayerDB(p, DB)
	}

	for _, p := range marray {
		player.InsertStatsDB(p, DB)
	}
	//fmt.Println(player.GetMazeStatsDB(DB))
}
