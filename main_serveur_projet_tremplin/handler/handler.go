package handler

import (
	"encoding/json"
	"fmt"
	"log"
	"main_serveur_projet_tremplin/page"
	"main_serveur_projet_tremplin/player"
	"net/http"
	"strings"
	"text/template"

	"github.com/bvinc/go-sqlite-lite/sqlite3"
)

//DBExtends struct to use it in handlers
type DBExtends struct {
	DB *sqlite3.Conn
}

//APIHandler api rest, return every students datas in the db
func (db DBExtends) APIHandler(w http.ResponseWriter, r *http.Request) {
	array := player.GetPlayersDB(db.DB)
	w.Header().Add("Content-Type", "application/json")
	str, err := json.Marshal(array)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Fprint(w, string(str))
}

//APIMazeStatsHandler api rest, return every students maze stats datas in the db
func (db DBExtends) APIMazeStatsHandler(w http.ResponseWriter, r *http.Request) {
	var array [][]player.MazeStat
	keys, ok := r.URL.Query()["student"]
	if !ok || len(keys[0]) < 1 {
		array = append(array, player.GetMazeStatsDB(db.DB))
		log.Println("Url Param 'student' is missing")
	} else {
		keys = strings.Split(keys[0], ",")
		for _, key := range keys {
			array = append(array, player.GetMazeStatsByStudentDB(db.DB, key))
		}
	}
	w.Header().Add("Content-Type", "application/json")
	str, err := json.Marshal(array)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Fprint(w, string(str))
}

//MainHandler main page
func MainHandler(w http.ResponseWriter, r *http.Request) {
	studentsNumber := r.URL.Path[len("/"):]
	page := page.Page{Title: studentsNumber, Body: "Bienvenue à vous !"}
	t, _ := template.ParseFiles("./web_tremplin/index.html")
	t.Execute(w, page)
}
