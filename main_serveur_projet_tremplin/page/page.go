package page

//Page struct
type Page struct {
	Title string
	Body  string
}
