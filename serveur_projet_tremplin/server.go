package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"os"
	"serveur_projet_tremplin/player"
	"strings"
	"time"
)

var labs []string

func init() {
	//Lit le fichier "labs.txt" qui contient le hash des labyrinthes que les éleves devront résoudre.
	file, err := os.Open("labs.txt")
	if err != nil {
		fmt.Print(err)
		os.Exit(1)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		labs = append(labs, scanner.Text())
	}

	if len(os.Args) > 1 {
		fmt.Println("Instance server with", os.Args[1])
		data, err := ioutil.ReadFile(os.Args[1])
		if err != nil {
			fmt.Println(err)
		} else {
			var parray []player.Player
			err = json.Unmarshal(data, &parray)
			for _, p := range parray {
				player.Players = append(player.Players, p)
			}
			if err != nil {
				fmt.Println(err)
			}
		}
	}
}

func main() {
	listener, err := net.Listen("tcp", ":8192")
	if err != nil {
		fmt.Println(err)
	}
	go timerServer()
	for {
		connexion, err := listener.Accept()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		go handleConnexion(connexion)
	}
}

func timerServer() {
	//Timer qui tout les 10 secondes va écrire dans un fichier "log.json" les résultats acutels.
	tickerLog := time.NewTicker(10 * time.Second)
	//Timer qui tout les 20 secondes va transmettre au serveur centrale les données qu'il contient.
	tickerMainServer := time.NewTicker(20 * time.Second)

	for {
		select {
		case <-tickerLog.C:
			str, _ := json.Marshal(player.Players)
			err := ioutil.WriteFile("log.json", []byte(str), 0644)
			if err != nil {
				fmt.Println(err)
			}
		case <-tickerMainServer.C:
			go sendToMainServer()
		}
	}
}

func handleConnexion(connexion net.Conn) {
	reader := bufio.NewReader(connexion)
	message, err := reader.ReadString('\n')
	if err != nil {
		if err == io.EOF {
			fmt.Println("Connexion close")
			return
		}
		fmt.Println(err)
		return
	}

	var p player.Player = player.GetPlayerFromJSON(message)
	if p.IsValid() {
		player.AddPlayerInTheArray(p)
		writer := bufio.NewWriter(connexion)
		if p.Status == 0 {
			writer.WriteString("next")
			writer.Flush()
			message, err = reader.ReadString('\n')
			message = strings.TrimSuffix(message, "\n")
			solve := player.GetSolveFromName(p.Name)
			if labs[solve%(len(labs)-1)] == message {
				player.AddStats(p)
				writer.WriteString(labs[(solve+1)%(len(labs)-1)])
			} else {
				writer.WriteString(labs[solve%(len(labs)-1)])
			}
			writer.Flush()
			fmt.Println(player.GetPlayerFromName(p.Name))
		} else {
			solve := player.GetSolveFromName(p.Name)
			writer.WriteString(labs[solve])
			writer.Flush()
		}
	}
	connexion.Close()
}

func sendToMainServer() {
	connexion, err := net.Dial("tcp", "127.0.0.1:6666")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer connexion.Close()
	fmt.Println("Open connection to the main server")
	writer := bufio.NewWriter(connexion)
	player.MutexArray.Lock()
	str, err := json.Marshal(player.Players)
	str2, err := json.Marshal(player.PlayersMazeStats)
	player.PlayersMazeStats = player.PlayersMazeStats[:0]
	player.MutexArray.Unlock()
	if err != nil {
		fmt.Println(err)
		return
	}
	writer.WriteString(string(str) + "\n")
	writer.Flush()
	writer.WriteString(string(str2) + "\n")
	writer.Flush()
}
