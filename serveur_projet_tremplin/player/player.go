package player

import (
	"encoding/json"
	"fmt"
	"sync"
)

var MutexArray sync.Mutex

//Player struct
type Player struct {
	Name       string  `json:"student"`
	Solve      int     `json:"solve"`
	Count      int     `json:"count"`
	Status     int     `json:"status"`
	OrientateR int     `json:"orientateR"`
	OrientateL int     `json:"orientateL"`
	Time       float64 `json:"time"`
}

//Players contains every player (student) for the contest.
var Players = make([]Player, 0)

//MazeStat struct
type MazeStat struct {
	Name  string `json:"student"`
	Maze  int    `json:"maze"`
	Count int    `json:"count"`
}

//PlayersMazeStats contains every player (student) for the contest.
var PlayersMazeStats = make([]MazeStat, 0)

//GetJSON return json of a player
func (player Player) GetJSON() string {
	fooMarshalled, err := json.Marshal(player)
	if err != nil {
		return "-1"
	}
	return string(fooMarshalled)
}

//GetPlayerFromJSON return a player thanks to a json
func GetPlayerFromJSON(JSON string) Player {
	var player Player
	data := []byte(JSON)
	err := json.Unmarshal(data, &player)
	if err != nil {
		fmt.Println(err)
		return Player{"nil", -1, -1, -1, -1, -1, -1.0}
	}
	return player
}

//IsValid determinate if the player is valid, in fact it just verify if the GetPlayerFromJSON function works well.
func (player Player) IsValid() bool {
	return !(player.Name == "nil" && player.Count == -1 && player.Status == -1 && player.OrientateR == -1 && player.OrientateL == -1 && player.Time == -1.0)
}

//AddPlayerInTheArray add the player in the array or update the player data if he already exists
func AddPlayerInTheArray(player Player) {
	defer MutexArray.Unlock()
	MutexArray.Lock()
	found := -1
	for index, p := range Players {
		if p.Name == player.Name {
			found = index
		}
	}
	if found == -1 {
		Players = append(Players, Player{player.Name, 0, 0, 0, 0, 0, 0.0})
	}
}

//AddStats add the player in the array or update the player data if he already exists
func AddStats(player Player) {
	defer MutexArray.Unlock()
	MutexArray.Lock()
	found := -1
	for index, p := range Players {
		if p.Name == player.Name {
			found = index
		}
	}
	if found == -1 {
		Players = append(Players, player)
	} else {
		p := Players[found]
		p.Count += player.Count
		p.OrientateR += player.OrientateR
		p.OrientateL += player.OrientateL
		p.Time += player.Time
		p.Solve++
		Players[found] = p
	}
	PlayersMazeStats = append(PlayersMazeStats, MazeStat{
		Name:  player.Name,
		Maze:  GetSolveFromName(player.Name),
		Count: player.Count,
	})
}

//GetPlayerFromName azdd
func GetPlayerFromName(name string) Player {
	for _, player := range Players {
		if player.Name == name {
			return player
		}
	}
	return Player{"nil", -1, -1, -1, -1, -1, -1.0}
}

//GetSolveFromName azfda
func GetSolveFromName(name string) int {
	for _, player := range Players {
		if player.Name == name {
			return player.Solve
		}
	}
	return -1
}
