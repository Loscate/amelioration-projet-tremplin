// 47.223379037336265,-1.5455371141433716
const DISTANCES = [{
        dist: 710,
        Start: "IUT de Nantes - Joffre",
        End: "Cathédrale Saint-Pierre-et-Saint-Paul de Nantes"
    },{
        dist: 1430,
        Start: "IUT de Nantes - Joffre",
        End: "CHU de Nantes"
    },{
        dist: 3920,
        Start: "IUT de Nantes - Joffre",
        End: "Stade de la Beaujoire - Nantes"
    },{
        dist: 6850,
        Start: "IUT de Nantes - Joffre",
        End: "Polytech Nantes"
    },{
        dist: 13350,
        Start: "IUT de Nantes - Joffre",
        End: "Treillières"
    },{
        dist: 25020,
        Start: "IUT de Nantes - Joffre",
        End: "Clisson"
    },{
        dist: 31960,
        Start: "IUT de Nantes - Joffre",
        End: "Ancenis"
    },{
        dist: 43910,
        Start: "IUT de Nantes - Joffre",
        End: "Pornic"
    },{
        dist: 67740,
        Start: "IUT de Nantes - Joffre",
        End: "Guérande"
    },{
        dist: 99280,
        Start: "IUT de Nantes - Joffre",
        End: "Rennes"
    },{
        dist: 169540,
        Start: "IUT de Nantes - Joffre",
        End: "Tours"
    },{
        dist: 169540,
        Start: "IUT de Nantes - Joffre",
        End: "Tours"
    },{
        dist: 210110,
        Start: "IUT de Nantes - Joffre",
        End: "Quimper"
    },{
        dist: 247900,
        Start: "IUT de Nantes - Joffre",
        End: "Crozon"
    },{
        dist: 298760,
        Start: "IUT de Nantes - Joffre",
        End: "Bourges"
    },{
        dist: 341980,
        Start: "IUT de Nantes - Joffre",
        End: "Paris"
    },{
        dist: 392890,
        Start: "IUT de Nantes - Joffre",
        End: "Dax (Landes)"
    },{
        dist: 405040,
        Start: "IUT de Nantes - Joffre",
        End: "Compiègne (Oise)"
    },{
        dist: 514690,
        Start: "IUT de Nantes - Joffre",
        End: "Lyon"
    },{
        dist: 613740,
        Start: "IUT de Nantes - Joffre",
        End: "Avignon"
    },{
        dist: 695220,
        Start: "IUT de Nantes - Joffre",
        End: "Marseille"
    },{
        dist: 790720,
        Start: "IUT de Nantes - Joffre",
        End: "Nice"
    },{
        dist: 986080,
        Start: "IUT de Nantes - Joffre",
        End: "TUM (Université technique de Munich) - Munich (Allemagne)"
    },{
        dist: 1342290,
        Start: "IUT de Nantes - Joffre",
        End: "Vienne (Autriche)"
    },{
        dist: 1600010,
        Start: "IUT de Nantes - Joffre",
        End: "Casablanca (Maroc)"
    },{
        dist: 2295680,
        Start: "IUT de Nantes - Joffre",
        End: "Athènes (Grèce)"
    },{
        dist: 3390420,
        Start: "IUT de Nantes - Joffre",
        End: "Le Caire (Égypte)"
    },{
        dist: 3533850,
        Start: "IUT de Nantes - Joffre",
        End: "Nuuk (Groenland)"
    },{
        dist: 5513270,
        Start: "IUT de Nantes - Joffre",
        End: "Dubai"
    },{
        dist: 6907290,
        Start: "IUT de Nantes - Joffre",
        End: "Delhi (Inde)"
    },{
        dist: 9601680,
        Start: "IUT de Nantes - Joffre",
        End: "Shanghai (Chine)"
    },{
        dist: 11058880,
        Start: "IUT de Nantes - Joffre",
        End: "Singapour (Singapour)"
    },{
        dist: 17302420,
        Start: "IUT de Nantes - Joffre",
        End: "Sydney (Australie)"
    }
];

export {DISTANCES};