"use strict";

import { DISTANCES } from './distance.js';

const updateProgressBar = (data, distances) => {
    let start = document.querySelector("#start");
    let end = document.querySelector("#end");
    let progressBar = document.querySelector("#barDist");
    let kmDist = document.querySelector("#kmDist");
    let paliers = document.querySelector("#paliers");
    let distance = getTotalDistance(data);
    console.log(distance);
    let done = false;
    paliers.innerHTML = "";
    distances.forEach((dist, index) => {
        if (!done && distance < dist.dist) {
            console.log(dist);
            start.innerHTML = dist.Start;
            end.innerHTML = dist.End;
            progressBar.setAttribute("style", `width: ${(distance/dist.dist) * 100}%`)
            kmDist.innerHTML = `${distance/1000}km parcouru (Distance entre "${dist.Start}" et "${dist.End}": ${dist.dist/1000}km)`;
            done = true;
        }

        if (!done) {
            paliers.innerHTML += `<tr><td>${index}</td><td>${dist.End}</td><td>${dist.dist/1000}</td><td><i class="material-icons">done</i></td</tr>`;
        }else {
            paliers.innerHTML += `<tr><td>${index}</td><td>${dist.End}</td><td>${dist.dist/1000}</td><td><i class="material-icons">remove</i></td</tr>`;
        }
    });
};

const getMazeStatsDataApi = (studentName, bests) => {
    return new Promise((resolve, reject) => {
        let urlArgument = studentName;
        bests.forEach(best => {
            if(urlArgument !== "") {
                urlArgument += ",";
            }
            urlArgument += best.student;
        });
        let datas = [];
        let average = [];
        fetch("http://localhost:8080/api/mazestats?student=" + urlArgument)
            .then(resp => resp.json())
            .then(data => {
                let jStartValue = 0;
                let divider = 0;
                if (studentName === "") {
                    datas.push(new Array(data[0].length).map(_ => 0));
                    divider = data.length;
                }else {
                    jStartValue = 1;
                    datas.push(data[0].map(element => element.count));
                    divider = data.length-1;
                }
                for(let i = 0; i < data[0].length; i++) {
                    let sum = 0;
                    for(let j = jStartValue; j < data.length; j++) {
                        sum += (data[j][i] === undefined ? 0 : data[j][i].count);   
                    }
                    average.push(sum/divider);
                }
                datas.push(average);
                resolve(datas);
            });
    });

};

const get3BestStudent = (data) => {
    data.sort((a, b) => b.solve - a.solve);
    return data.slice(0, 3)
};

const getDataApi = () => {
    return new Promise((resolve, reject) => {
        fetch("http://localhost:8080/api")
            .then(resp => resp.json())
            .then(data => resolve(data))
            .catch(err => reject(err));
    });
    
};

const getPlayerDistance = (player) => {
    return parseFloat(player.count * 1.5).toFixed(2);
};

const getPlayerSpeed = (player) => {
    return parseFloat(getPlayerDistance(player) / player.time).toFixed(2);
};

const updatePersonnalPlayer = (data, studentName) => {
    let found = false;
    data.forEach(stud => {
        if(studentName === stud.student){
            document.querySelector(".students_count").innerHTML = stud.student + " : " + stud.solve;
            found = true;
        }
    });
    if(!found){
        document.querySelector(".students_count").innerHTML = "...";
    }
};

const updateDataTime = (data) => {
    let dataTime = [...data];
    dataTime.sort((a, b) => b.time - a.time);
    switch (dataTime.length) {
        case 0:
            break;
        case 1:
            document.querySelector("#time-1").innerHTML = dataTime[0].student + " : " + parseFloat(dataTime[0].time).toFixed(2) + " s.";
            break;
        case 2:
            document.querySelector("#time-1").innerHTML = dataTime[0].student + " : " + parseFloat(dataTime[0].time).toFixed(2) + " s.";
            document.querySelector("#time-2").innerHTML = dataTime[1].student + " : " + parseFloat(dataTime[1].time).toFixed(2) + " s.";
            break;
        default:
            document.querySelector("#time-1").innerHTML = dataTime[0].student + " : " + parseFloat(dataTime[0].time).toFixed(2) + " s.";
            document.querySelector("#time-2").innerHTML = dataTime[1].student + " : " + parseFloat(dataTime[1].time).toFixed(2) + " s.";
            document.querySelector("#time-3").innerHTML = dataTime[2].student + " : " + parseFloat(dataTime[2].time).toFixed(2) + " s.";
            break;
    }
};

const updateDataDist = (data) => {
    let dataDist = [...data];
    dataDist.sort((a, b) => getPlayerDistance(b) - getPlayerDistance(a));
    switch (dataDist.length) {
        case 0:
            break;
        case 1:
            document.querySelector("#dist-1").innerHTML = dataDist[0].student + " : " + getPlayerDistance(dataDist[0]) + " m.";
            break;
        case 2:
            document.querySelector("#dist-1").innerHTML = dataDist[0].student + " : " + getPlayerDistance(dataDist[0]) + " m.";
            document.querySelector("#dist-2").innerHTML = dataDist[1].student + " : " + getPlayerDistance(dataDist[1]) + " m.";
            break;
        default:
            document.querySelector("#dist-1").innerHTML = dataDist[0].student + " : " + getPlayerDistance(dataDist[0]) + " m.";
            document.querySelector("#dist-2").innerHTML = dataDist[1].student + " : " + getPlayerDistance(dataDist[1]) + " m.";
            document.querySelector("#dist-3").innerHTML = dataDist[2].student + " : " + getPlayerDistance(dataDist[2]) + " m.";
            break;
    }
};

const updateDataSpeed = (data) => {
    let dataSpeed = [...data];
    dataSpeed.sort((a, b) => getPlayerSpeed(b) - getPlayerSpeed(a));
    switch (dataSpeed.length) {
        case 0:
            break;
        case 1:
            document.querySelector("#speed-1").innerHTML = dataSpeed[0].student + " : " + getPlayerSpeed(dataSpeed[0]) + " m/s.";
            break;
        case 2:
            document.querySelector("#speed-1").innerHTML = dataSpeed[0].student + " : " + getPlayerSpeed(dataSpeed[0]) + " m/s.";
            document.querySelector("#speed-2").innerHTML = dataSpeed[1].student + " : " + getPlayerSpeed(dataSpeed[1]) + " m/s.";
            break;
        default:
            document.querySelector("#speed-1").innerHTML = dataSpeed[0].student + " : " + getPlayerSpeed(dataSpeed[0]) + " m/s.";
            document.querySelector("#speed-2").innerHTML = dataSpeed[1].student + " : " + getPlayerSpeed(dataSpeed[1]) + " m/s.";
            document.querySelector("#speed-3").innerHTML = dataSpeed[2].student + " : " + getPlayerSpeed(dataSpeed[2]) + " m/s.";
            break;
    }
};

const updateDataOrientate = (data) => {

    let dataR = [...data];
    let dataO = [...data];
    let dataL = [...data];
    dataR.sort((a, b) => b.orientateR - a.orientateR);
    dataO.sort((a, b) => (b.orientateR + b.orientateL) - (a.orientateR + a.orientateL));
    dataL.sort((a, b) => b.orientateL - a.orientateL);
    switch (data.length) {
        case 0:
            break;
        case 1:
            document.querySelector("#orientate-right-1").innerHTML = dataR[0].student + " : " + dataR[0].orientateR + " fois.";

            document.querySelector("#orientate-1").innerHTML = dataO[0].student + " : " + (dataO[0].orientateR + dataO[0].orientateL) + " fois.";

            document.querySelector("#orientate-left-1").innerHTML = dataL[0].student + " : " + dataL[0].orientateL + " fois.";
            break;
        case 2:
            document.querySelector("#orientate-right-1").innerHTML = dataR[0].student + " : " + dataR[0].orientateR + " fois.";
            document.querySelector("#orientate-right-2").innerHTML = dataR[1].student + " : " + dataR[1].orientateR + " fois.";

            document.querySelector("#orientate-1").innerHTML = dataO[0].student + " : " + (dataO[0].orientateR + dataO[0].orientateL) + " fois.";
            document.querySelector("#orientate-2").innerHTML = dataO[1].student + " : " + (dataO[1].orientateR + dataO[1].orientateL) + " fois.";

            document.querySelector("#orientate-left-1").innerHTML = dataL[0].student + " : " + dataL[0].orientateL + " fois.";
            document.querySelector("#orientate-left-2").innerHTML = dataL[1].student + " : " + dataL[1].orientateL + " fois.";
            break;
        default:
            document.querySelector("#orientate-right-1").innerHTML = dataR[0].student + " : " + dataR[0].orientateR + " fois.";
            document.querySelector("#orientate-right-2").innerHTML = dataR[1].student + " : " + dataR[1].orientateR + " fois.";
            document.querySelector("#orientate-right-3").innerHTML = dataR[2].student + " : " + dataR[2].orientateR + " fois.";

            document.querySelector("#orientate-1").innerHTML = dataO[0].student + " : " + (dataO[0].orientateR + dataO[0].orientateL) + " fois.";
            document.querySelector("#orientate-2").innerHTML = dataO[1].student + " : " + (dataO[1].orientateR + dataO[1].orientateL) + " fois.";
            document.querySelector("#orientate-3").innerHTML = dataO[2].student + " : " + (dataO[2].orientateR + dataO[2].orientateL) + " fois.";

            document.querySelector("#orientate-left-1").innerHTML = dataL[0].student + " : " + dataL[0].orientateL + " fois.";
            document.querySelector("#orientate-left-2").innerHTML = dataL[1].student + " : " + dataL[1].orientateL + " fois.";
            document.querySelector("#orientate-left-3").innerHTML = dataL[2].student + " : " + dataL[2].orientateL + " fois.";

            break;
    }


};

const getTotalDistance = (data) => {
    let dist = 0;
    data.forEach(element => {
        dist += parseFloat(getPlayerDistance(element));
    });
    return dist;
};

const updateDataChartPodium = (chart, data) => {
    if(data != undefined && chart != undefined){
        data.sort((a, b) => b.solve - a.solve);
        switch (data.length) {
            case 0:
                break;
            case 1:
                chart.data.datasets[0].data[1] = data[0].solve;
                break;
            case 2:
                
                chart.data.datasets[0].data[1] = data[0].solve;
                chart.data.datasets[0].data[0] = data[1].solve;
                break;
            default:
        
                chart.data.datasets[0].data[1] = data[0].solve;
                chart.data.datasets[0].data[0] = data[1].solve;
                chart.data.datasets[0].data[2] = data[2].solve;
                break;
        }
        chart.update();
    }
};

const updateDataChartGraph = (chart, data, min, max) => {
    if(data != undefined && chart != undefined){
        chart.data.labels =  [...Array(max-min)].map((_, i) => i + min);
        chart.data.datasets[0].data = data[0].slice(min, max);
        chart.data.datasets[1].data = data[1].slice(min, max);
        chart.update();
    }
};

let podiumChart;
let graphChart;
let slider;
let dataChart;
let minChart = 0;
let maxChart = 0;

document.addEventListener('DOMContentLoaded', function() {
    let studentName = window.location.pathname.substring(1);
    let elems = document.querySelectorAll('.pushpin');
    let instances = M.Pushpin.init(elems, {});

    slider = document.getElementById('slider');

    noUiSlider.create(slider, {
        start: [0, 100],
        connect: true,
        step: 1,
        range: {
            'min': 0,
            "25%": 25,
            "50%": 50,
            "75%": 75,
            'max': 100
        },
        pips: {
            mode: 'range',
            density: 2
        }
    });

    let podium = document.getElementById("podium").getContext("2d");
    podiumChart = new Chart(podium, {
        type: 'bar',
        data: {
            labels: [
                "Deuxieme",
                "Premier",
                "Troisieme"
            ],
            datasets: [{
                label: 'maze solve',
                barPercentage: 1,
                minBarLength: 2,
                data: [0, 0, 0],
                backgroundColor: [
                    'rgba(191, 200, 206, 0.2)',
                    'rgba(240, 253, 40, 0.2)',
                    'rgba(218, 128, 106, 0.2)'
                ],
                borderColor: [
                    'rgba(191, 200, 206, 1)',
                    'rgba(240, 253, 40, 1)',
                    'rgba(218, 128, 106, 1)'
                ],
                borderWidth: 2
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });


    let graph = document.getElementById("graph").getContext("2d");
    graphChart = new Chart(graph, {
        type: 'line',
        data: {
            labels: [0],
            datasets: [{
                label: "Nombre de déplacements par labyrinthe personnel",
                data: [0],
                fill: false,
                borderColor: "#ff0000"
            },{
                label: "Moyenne des meilleurs étudiants",
                data: [0],
                fill: false,
                borderColor: "#0000ff"
            }]
        },
        options: {
        }
    });

    getDataApi()
    .then(data => {
        let bests = get3BestStudent(data);

        getMazeStatsDataApi(studentName, bests)
        .then(dataStats => {
            dataChart = dataStats;
            maxChart = dataStats[0].length;
            slider.noUiSlider.updateOptions({
                start: [minChart, maxChart]
            });
            updateDataChartGraph(graphChart, dataStats, minChart, maxChart);
            slider.noUiSlider.updateOptions({
                range: {
                    'min': 0,
                    "25%": parseInt(maxChart*0.25),
                    "50%": parseInt(maxChart*0.50),
                    "75%": parseInt(maxChart*0.75),
                    'max': maxChart
                }
            });
            slider.noUiSlider.set([minChart, maxChart]);
        });
        updateDataChartPodium(podiumChart, data);
        updateDataOrientate(data);
        updateDataSpeed(data);
        updateDataDist(data);
        updatePersonnalPlayer(data);
        updateDataTime(data);
        updateProgressBar(data, DISTANCES);
    })
    setInterval(() => {
        getDataApi()
            .then(data => {
                let bests = get3BestStudent(data);
                getMazeStatsDataApi(studentName, bests)
                .then(dataStats => {
                    dataChart = dataStats;
                    updateDataChartGraph(graphChart, dataStats, minChart, maxChart);
                    slider.noUiSlider.updateOptions({
                        range: {
                            "min": 0,
                            '25%': parseInt(dataStats[0].length*0.25),
                            '50%': parseInt(dataStats[0].length*0.50),
                            '75%': parseInt(dataStats[0].length*0.75),
                            "max": dataStats[0].length
                        }
                    });
                });
                updateDataChartPodium(podiumChart, data);
                updateDataOrientate(data);
                updateDataSpeed(data);
                updateDataDist(data);
                updatePersonnalPlayer(data, studentName);
                updateDataTime(data);
                updateProgressBar(data, DISTANCES);
                
            })
    }, 20 * 1000);
    
    slider.noUiSlider.on('slide', function (values, handle) {
        minChart = parseInt(values[0]);
        maxChart = parseInt(values[1]);
        updateDataChartGraph(graphChart, dataChart, minChart, maxChart);
    });
});