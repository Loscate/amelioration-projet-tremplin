# Projet d'amelioration du "Projet Tremplin"

> The "Projet Tremplin" is an educational activity offered by the University of Nantes tostudents
> entering the DUT Informatique. It consists of solving a series of labyrinths using algorithms
> written in the C language, all on a Linux environment, mainly used during the course. Students
> will have to work with a server that stores mazes, and must also cooperate with other students to
> complete this project.
>
> We develop this procject as part of the IUT of Nantes in order to improve students experience,
> to make it easier for teachers and make the first week of lessons even more immersive and
> interesting. To do that we start from to do this we just have the statement of needs and an
> imposed programing language C, but we had to rebuilt completely the software from nothing.
> The goal for students is to discover the basics of programing and the linux environment.
> This project lasts one week to finish with a contest where student will have to solve
> as many as possible maze in one hour.
>
> The hardest part was to hide all complex data structures from students, because they only have to
> use simple types. We develop the project in three main parts :
>
> * maze(with all methods to make it usable)
>
> * display(to have a correct and pleasant display with many options)
>
> * server(use for the contest witch have to stock mazes parts, send the good maze to the student
> and collect data)

## Table of content

- [Projet d'amelioration du "Projet Tremplin"](#projet-damelioration-du-%22projet-tremplin%22)
  - [Table of content](#table-of-content)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installing](#installing)
    - [Playing](#playing)
    - [Options](#options)
  - [Built_With](#builtwith)
  - [Contributing](#contributing)
  - [Authors](#authors)
  - [License](#license)
  - [Acknowledgments](#acknowledgments)

## Getting Started

if you want to have this project on your computer :

```html
git clone https://gitlab.com/Loscate/amelioration-projet-tremplin.git
```

or
download the project

### Prerequisites

* Ncurses

### Installing

Step 1 : download project

```html
Download or clone the project
```

Step 2 : open the folder

```html
open a terminal in the folder
```

Step 3 : compile the code

```html
~$ make
```

Step 4 : run the program with the following command

```html
~$ ./ProjetTremplin
```

Step 5 : if you want to exit the program

```html
exit the program with Ctrl + C or taping any key when the player has reached the end
```

if you want you can clear your folder by doing this command :

```html
~$ make clean
```

### Playing

Course of the game :

To succeed in finishing a labyrinth you have to carry out a program in the file "resolutionLabyrinthe.c" using the functions already coded to move the player in the labyrinth and reach the finish.

with the keyboard resolution you have to use key_UP to move forward, key_LEFT to turn left and
key_RIGHT to turn right.

when you reached the arrival you can use key_UP and key_DOWN or mouse wheel to sroll in the terminal.

in order to solve the maze you have six functions you can use :

* **is_solvable()** :
to know if the maze can be resolve.

* **is_move_forward_possible()** :
to know if you can move in front of you

* **move_forward()** :
to move in the direction of the player

* **turn_left()** :
to change orientation of the player to the left

* **turn_right()** :
to change orientation of the player to the right

* **print_terminal()** :
to print anything you want in the terminal

* **write_coord_terminal()** :
to print coordinate in the terminal

### Options

* *-d mode* : change display mode

  0 : classic display (default)

  1 : footstep display

  2 : front view display

  9 : no display

* *-a algorithm* : change algorithm

  0 : left hand algorithm (default)

  1 : pledge algorithm

  2 : personnal algorithm

  9 : keyboard resolution

* *-g generate* : generate maze

  1 : generate maze with roots

  2 : generate maze completely randomly

* *--speed step* : change speed execution

  0 : no time between move

  1 : 10ms time between move

  2 : 100ms time between move

  other : 25ms time between move (default)

* *--start Coordinates* (x,y) : change start coordinate

* *--arrival Coordinates* (x,y) : change arrival coordinate

* *--start-random* : change start coordinate randomly

* *--arrival-random* : change start coordinate randomly

* *-h* : display this help

## Built_With

* [C](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf) - *programming language*

* [Go](https://golang.org/) - *programming language*

* [Ncurses](http://ftp.gnu.org/pub/gnu/ncurses/) - *library for display*

* [GCC](https://gcc.gnu.org/) - *library for compiler*

## Contributing

Loïg Jeszequel - lead teacher

## Authors

 **Simon Perrin** - **Albert Guillard** - **Alexis Guillotin**

## License

 IUT of Nantes - DUT informatique

## Acknowledgments

* Loïg Jezequel
* Simon Sassi
* Baptiste Batard
* Eliott Dubois
